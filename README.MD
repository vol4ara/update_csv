### Before run project
```
pip install -r requirements.txt
python manage.py migrate
```
### Run tests

```
python manage.py test
```
