import os
from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status

from updateCsv import TMP_FILE


class TestFunc(APITestCase):

    @classmethod
    def tearDownClass(cls):
        os.remove(TMP_FILE)

    def test_get_data(self):
        url = reverse('api-flight')
        data = {
            "name": "A14"
        }
        response = self.client.get(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'name': 'A14', 'arrival': ' 12:00', 'departure': ' 19:00 ', 'state': '’’'})

    def test_update_data(self):
        url = reverse('api-flight')
        data = {
            "name": "A14",
            "arrival": "00:01",
            "departure": "10:00"
        }

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'name': 'A14', 'arrival': '00:01', 'departure': '10:00', 'state': 'success'})

    def test_add_new_data(self):
        url = reverse('api-flight')
        data = {
            "name": "A222",
            "arrival": "00:02",
            "departure": "02:00"
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'name': 'A222', 'arrival': '00:02', 'departure': '02:00', 'state': 'failed'})

    def test_arrival_only(self):
        url = reverse('api-flight')
        data = {
            "name": "A222",
            "arrival": "00:02",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
