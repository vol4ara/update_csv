from unittest import TestCase

from api.business.utils import get_state


class TestFunc(TestCase):
    
    def test_success_state(self):
        self.assertEqual('success', get_state("01:00", "15:00"))

    def test_failed_state(self):
        self.assertEqual('failed', get_state("01:00", "02:00"))
