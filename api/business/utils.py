from datetime import datetime


def convert_time(time_string):
    return datetime.strptime(time_string.strip(), '%H:%M')


def get_state(arrival, departure):
    time_to_compare = convert_time('04:00') - convert_time('01:00')
    diff_time = convert_time(departure) - convert_time(arrival)
    if diff_time > time_to_compare:
        return "success"
    return "failed"


def flight_row_to_dict(flight_row):
    return {
        "name": flight_row[0],
        "arrival": flight_row[1],
        "departure": flight_row[2],
        "state": flight_row[3]
    }
