import csv
import shutil
import time
from tempfile import NamedTemporaryFile
from filelock import FileLock
from api.serializers import FlightSerializer
from updateCsv.settings import CSV_FILE_PATH


def get_csv_data():
    with open(CSV_FILE_PATH, 'r') as f:
        reader = csv.reader(f)
        data = list(reader)
    return data


def get_flight_row(flight_from: FlightSerializer.data) -> dict:
    data = get_csv_data()
    for cur_line in data:
        if flight_from['name'] in cur_line[0]:
            return cur_line


def write_to_csv(flight_row):
    '''
    :param flight_row: list 
    :return: int
    This function is for the writing data to csv
    '''
    data = get_csv_data()
    tmp_file = NamedTemporaryFile().name
    # we need lock to make file consistent
    lock = FileLock(CSV_FILE_PATH + ".lock")
    with lock:
        with open(tmp_file, 'w+') as f:
            writer = csv.writer(f)
            for i, flight in enumerate(data):
                if flight_row[0] in flight[0]:
                    data[i] = flight_row
                    writer.writerows(data)
                    break
            else:
                data.append(flight_row)
                writer.writerows(data)
        shutil.move(tmp_file, CSV_FILE_PATH)
