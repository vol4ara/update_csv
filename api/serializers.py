from rest_framework import serializers


class FlightSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=15)
    arrival = serializers.TimeField(format='%H:%M', required=False)
    departure = serializers.TimeField(format='%H:%M', required=False)
    state = serializers.CharField(max_length=15, required=False, read_only=True)

    def to_list(self):
        return [self.data['name'], self.data.get('arrival'), self.data.get('departure'), '']
