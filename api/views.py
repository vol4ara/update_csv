from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from api.business.utils import get_state, flight_row_to_dict
from api.business.work_with_cvs import get_flight_row, write_to_csv
from api.serializers import FlightSerializer


class FlightView(APIView):
    http_method_names = ['get', 'post']

    def get(self, request):
        flight_sz = FlightSerializer(data=request.query_params)
        flight_sz.is_valid(raise_exception=True)
        row_list = get_flight_row(flight_sz.data)
        data = {
            "name": row_list[0],
            "arrival": row_list[1],
            "departure": row_list[2],
            "state": row_list[3]
        }
        return Response(status=status.HTTP_200_OK, data=data)

    def post(self, request):
        flight_sz = FlightSerializer(data=request.data)
        flight_sz.is_valid(raise_exception=True)
        arrival = flight_sz.data.get('arrival')
        departure = flight_sz.data.get('departure')
        # check if miss any data
        if not arrival:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"error": "arrival time needed"})
        elif not departure:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"error": "departure time needed"})
        flight_list = flight_sz.to_list()
        flight_list[3] = get_state(arrival, departure)
        err = write_to_csv(flight_list)
        if not err:
            return Response(status=status.HTTP_200_OK, data=flight_row_to_dict(flight_list))
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': err})
