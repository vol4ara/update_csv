from django.urls import path
from rest_framework import routers

from api.views import FlightView

router = routers.DefaultRouter()

urlpatterns = [
    path('flight/', FlightView.as_view(), name='api-flight')

]
