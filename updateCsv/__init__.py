import shutil
import sys
from tempfile import NamedTemporaryFile

from updateCsv import settings


if 'test' in sys.argv:
    TMP_FILE = NamedTemporaryFile(delete=False).name
    shutil.copy('tests/flights_test.csv', TMP_FILE)
    NEW_SETTINGS = {
        "CSV_FILE_PATH": TMP_FILE
    }
    settings.__dict__.update(NEW_SETTINGS)
